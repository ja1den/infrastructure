{inputs, ...}: {
  imports = [
    ./interpreter.nix
  ];

  perSystem = {pkgs, ...}: {
    devShells = {
      "default" = inputs.devenv.lib.mkShell {
        inherit inputs pkgs;

        modules = [
          {
            languages = {
              ansible = {
                enable = true;
              };

              nix = {
                enable = true;
              };
            };

            packages = [
              pkgs.sops

              pkgs.nix-diff

              pkgs.python3

              pkgs.shellcheck
            ];
          }
        ];
      };
    };
  };
}
