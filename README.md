# Nimbus

> The configuration for my home cloud.

## Introduction

This respository contains the [Ansible](https://ansible.com/) playbook for my home cloud.

### Services

The playbook will configure the following services:

| Service                                                          | Domain                  | Default Port/s |
| ---------------------------------------------------------------- | ----------------------- | -------------- |
| [Authelia](https://authelia.com/)                                | `auth.example.com`      |                |
| [Inadyn](https://github.com/troglobit/inadyn)                    |                         |                |
| [LLDAP](https://github.com/nitnelave/lldap)                      | `ldap.example.com`      |                |
| [Traefik](https://doc.traefik.io/traefik/)                       | `traefik.example.com`   | `80`, `443`    |
| [dashdot](https://getdashdot.com/)                               | `dash.example.com`      |                |
| [Flame](https://github.com/pawelmalak/flame)                     | `example.com`           |                |
| [Nextcloud](https://docs.linuxserver.io/images/docker-nextcloud) | `nextcloud.example.com` |                |
| [Vaultwarden](https://github.com/dani-garcia/vaultwarden)        | `vault.example.com`     |                |
| [Webhook](https://github.com/thecatlady/docker-webhook)          | `hooks.example.com`     |                |

## Usage

### Deployment

Deploy all services:

```sh
./scripts/deploy.sh
```

Deploy a specific service:

```sh
./scripts/deploy.sh --tags nextcloud
```

This is most useful when you've changed the configuration for a single service, and it needs to be updated.

### Development

Lint the playbook with [`yamllint`](https://yamllint.readthedocs.io/en/stable/index.html) and [Ansible Lint](https://ansible-lint.readthedocs.io/):

```sh
./scripts/lint.sh
```

Encrypt a variable:

```shell
ansible-vault encrypt_string "S3CR3T_T0K3N"
```

List [Docker](https://docker.com/) containers:

```sh
watch -n 1 'docker ps -a --format "table {{.Image}}\t{{.RunningFor}}\t{{.Status}}\t{{.Names}}"'
```
